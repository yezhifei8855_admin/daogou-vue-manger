import req from '@/utils/system/req'

// 登录api
export function loginApi(data) {
  return req({
    url: '/user/login',
    method: 'post',
    data
  })
}

// 获取用户信息Api
export function getInfoApi(data) {
  return req({
    url: '/user/info',
    method: 'post',
    data
  })
}

// 退出登录Api
export function loginOutApi() {
  return req({
    url: '/user/logout',
    method: 'post',
  })
}

// 获取用户信息Api
export function passwordChange(data) {
  return req({
    url: '/user/passwordChange',
    method: 'post',
    data
  })
}
