import Layout from '@/layout/index.vue'
import { createNameComponent } from '../createNode'

/**
 * path: 路由地址，必填项
   component: 使用的组件，必填项
   redirect: 重定向地址，非必填
   alwayShow: 布尔类型，当子级只有一个时，是否展示父级菜单，true表示总是展示，false表示不展示，默认为false
   meta: 基础元数据，是一个对象，包括了下列属性
   title: 非国际化版本为标题，国际化版本为可供$t函数调用的对象层级字符串，必填项
   icon: 图标，非必填
   cache: 布尔类型，是否开启缓存功能，true表示开启，false表示关闭，默认为false, 非必填，目前仅支持在二级菜单上使用，暂不支持三级及三级以上菜单使用（社区难题，有待解决）
   自定义元数据，用于在其它定制化场景使用
   children: 子路由（如果需要子路由，父级的comonent需要为Layout组件/Menu组件（在router/modules/menu.ts文件中有示范）/自定义父级组件）
 * @type {({redirect: string, path: string, component, children: [{path: string, component: function(): Promise<unknown> | Promise<unknown>, meta: {cache: boolean, roles: [string, string], title: string}}, {path: string, component: function(): Promise<unknown> | Promise<unknown>, meta: {cache: boolean, roles: [string], title: string}}, {path: string, component: function(): Promise<unknown> | Promise<unknown>, meta: {cache: boolean, title: string}}], meta: {icon: string, title: string}, alwayShow: boolean}|{redirect: string, path: string, component, children: [{path: string, component: function(): Promise<unknown> | Promise<unknown>, meta: {cache: boolean, roles: [string, string], title: string}}, {path: string, component: function(): Promise<unknown> | Promise<unknown>, meta: {cache: boolean, roles: [string], title: string}}, {path: string, component: function(): Promise<unknown> | Promise<unknown>, meta: {cache: boolean, title: string}}], meta: {icon: string, title: string}, alwayShow: boolean})[]}
 */
const route = [
  {
    path: '/pages',
    component: Layout,
    redirect: '/pages/crudTable',
    meta: { title: '页面', icon: 'el-icon-document-copy' },
    alwayShow: true,
    children: [
      {
        path: 'crudTable',
        component: createNameComponent(() => import('@/views/main/pages/crudTable/index.vue')),
        meta: { title: '业务表格', cache: false, roles: ['admin', 'editor'] }
      },
      {
        path: 'categoryTable',
        component: createNameComponent(() => import('@/views/main/pages/categoryTable/index.vue')),
        meta: { title: '分类联动表格', cache: true, roles: ['admin'] }
      },
      {
        path: 'treeTable',
        component: createNameComponent(() => import('@/views/main/pages/treeTable/index.vue')),
        meta: { title: '树联动表格', cache: true }
      }
    ]
  },
  {
    path: '/pages/order',
    component: Layout,
    redirect: '/pages/order/index',
    meta: { title: '订单', icon: 'el-icon-document-copy' },
    alwayShow: true,
    children: [
      {
        path: 'index',
        component: createNameComponent(() => import('@/views/main/pages/order/index.vue')),
        meta: { title: '归集订单', cache: false, roles: ['admin', 'editor'] }
      },
      {
        path: 'pdd',
        component: createNameComponent(() => import('@/views/main/pages/order/pdd.vue')),
        meta: { title: '拼多多', cache: false, roles: ['admin', 'editor'] }
      },
      {
        path: 'tb',
        component: createNameComponent(() => import('@/views/main/pages/order/tb.vue')),
        meta: { title: '淘宝', cache: false, roles: ['admin', 'editor'] }
      },
    ]
  }
]

export default route