

## 简介

- [更新日志](./VERSION.md)
- 本项目是导购后台管理，后台php代码：https://github.com/brisk21/shopping-guide
- 项目客户端是基于uniapp开发的：https://gitee.com/brisklan/shopping-guide-app
- 此开源项目为个人开发，不限制任何商业使用和个人研究，使用之前请先点个Star对我进行鼓励
- 这是基于开源前端框架http://vue-admin-box-template.51weblove.com/js-i18n 开发的后台管理
- 个人经历有限，偶尔更新

## 预览

- [uniapp](http://h5.dg.wei1.top/#/)

- [管理端] 开发中

## 使用

1. 获取源码资源包

   

2. 安装依赖，国内推荐使用cnpm或tyarn，国外推荐使用npm或yarn

   ```
   npm install
   ```

   

3. 运行

   ```
   npm run dev 或 npm run start
   ```

   

4. 打包

   ```
   npm run build
   ```
   

